import time


class Payment:
    authorization_number = None
    amount = None
    invoice = None
    order = None
    payment_method = None
    paid_at = None

    def __init__(self, attributes={}):
        self.authorization_number = attributes.get('attributes', None)
        self.amount = attributes.get('amount', None)
        self.invoice = attributes.get('invoice', None)
        self.order = attributes.get('order', None)
        self.payment_method = attributes.get('payment_method', None)

    def pay(self, paid_at=time.time()):
        self.amount = self.order.total_amount
        self.authorization_number = int(time.time())
        attributes = dict(
            billing_address=self.order.address,
            shipping_address=self.order.address,
            order=self.order
        )
        self.invoice = Invoice(attributes=attributes)
        self.paid_at = paid_at
        self.order.close(self.paid_at)

    def is_paid(self):
        return self.paid_at != None


class Invoice:
    billing_address = None
    shipping_address = None
    order = None

    def __init__(self, attributes={}):
        self.billing_address = attributes.get('billing_address', None)
        self.shipping_address = attributes.get('shipping_address', None)
        self.order = attributes.get('order', None)


class Order:
    customer = None
    items = None
    payment = None
    address = None
    closed_at = None

    def __init__(self, customer, attributes={}):
        self.customer = customer
        self.items = []
        self.order_item_class = attributes.get('order_item_class', OrderItem)
        self.address = attributes.get('address', Address(zipcode='45678-979'))

    def add_product(self, product):
        self.items.append(self.order_item_class(order=self, product=product))

    def total_amount(self):
        total = 0
        for item in items:
            total += item.total

        return total

    def close(self, closed_at=time.time()):
        self.closed_at = closed_at

    # remember: you can create new methods inside those classes to help you create a better design


class OrderItem:
    order = None
    product = None

    def __init__(self, order, product):
        self.order = order
        self.product = product

    def total(self):
        return 10


class Product:
    # use type to distinguish each kind of product: physical, book, digital, membership, etc.
    name = None
    typ = None

    def __init__(self, name, typ):
        self.name = name
        if typ == 'MEMBERSHIP' or typ == 'DIGITAL' or typ == 'PHYSICAL' or typ == 'BOOK':
            self.typ = typ
        else:
            print('Não foi possível instanciar o tipo de produto inserido')   


class Address:
    zipcode = None

    def __init__(self, zipcode):
        self.zipcode = zipcode


class CreditCard:

    @staticmethod
    def fetch_by_hashed(code):
        return CreditCard()


class Customer:
    # you can customize this class by yourself
    #pass
    identifier = None
    name = None
    email = None
    
    def __init__(self, identifier, name, email):
        self.identifier = identifier
        self.name = name
        self.email = email
        

class Membership:
    TYPE_PRODUCT = 'MEMBERSHIP'
    member = Customer
    active = False

    def __init__(self, TYPE_PRODUCT, member):
        self.TYPE_PRODUCT = TYPE_PRODUCT
        self.member = member
    
    def sendEmail(self, Customer):
        #envia email personalizado para clientes do produto digital
        pass  

    def treatment(self):
        self.sendEmail(Customer)
        self.active = True
        print("EMAIL ENVIADO INFORMANDO QUE SUA CONTA FOI ATIVADA!")        


class Physical:
    TYPE_PRODUCT = 'PHYSICAL'
    member = Customer

    def __init__(self, TYPE_PRODUCT, member):
        self.TYPE_PRODUCT = TYPE_PRODUCT
        self.member = member

    def treatment(self):
        print("GERANDO SHIPPING LABEL...")


class Book:
    TYPE_PRODUCT = 'BOOK'
    member = Customer

    def __init__(self, TYPE_PRODUCT, member):
        self.TYPE_PRODUCT = TYPE_PRODUCT
        self.member = member

    def treatment(self):
        print("GERANDO SHIPPING LABEL..."+
             "\nESTE PRODUTO É ISENTO DE IMPOSTOS" +
             " CONFORME DISPOSTO NA CONSTITUIÇÃO ART.150, VI, d")


class Digital:
    TYPE_PRODUCT = 'DIGITAL'
    member = Customer
    voucher = 0

    def __init__(self, TYPE_PRODUCT, member):
        self.TYPE_PRODUCT = TYPE_PRODUCT
        self.member = member
    
    def sendEmail(self, Customer):
        #envia email personalizado para clientes do produto digital
        pass

    def grantDiscount(self):
        self.voucher = 10

    def treatment(self):
        self.sendEmail(Customer)
        print("EMAIL ENVIADO COM A DESCRIÇÃO DA COMPRA! VOCÊ" +
            " GANHOU UM VOUCHER COM 10% DE DESCONTO!")
        

# Book Example (build new payments if you need to properly test it)
def main():
    try:
        foolano = Customer(123, 'Carol', 'carolinevianapereira@gmail.com')
        book = Product(name='Awesome book', typ='MEMBERSHIP')
        book_order = Order(foolano)
        book_order.add_product(book)

        attributes = dict(
            order=book_order,
            payment_method=CreditCard.fetch_by_hashed('43567890-987654367')
        )
        payment_book = Payment(attributes=attributes)
        payment_book.pay()
        print(payment_book.is_paid())  # < true
        print(payment_book.order.items[0].product.typ)
        treatments = {'MEMBERSHIP': Membership, 'DIGITAL': Digital, 'PHYSICAL': Physical, 'BOOK': Book}
        call_treatment = treatments[book.typ](book.typ, foolano)
        call_treatment.treatment()

    except:
        print("Não foi possível construir esse objeto")

main()






# now, how to deal with shipping rules then?